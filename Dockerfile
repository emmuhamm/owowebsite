FROM node:18-alpine
WORKDIR /owowebsite/

COPY public/ /owowebsite/public
COPY src/ /owowebsite/src
COPY package.json /owowebsite/
RUN npm install

CMD ["npm", "start"]